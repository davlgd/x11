# Copyright 2011 Elias Pipping <pipping@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

MY_PNV=${PN}-$(ever replace 3 '-')

require autotools [ supported_autoconf=[ 2.7 ] supported_automake=[ 1.17 ] ]

export_exlib_phases src_prepare

SUMMARY="Cross-platform C++ GUI toolkit"
DESCRIPTION="
Provides modern GUI functionality without the bloat and supports 3D graphics via OpenGL and its
built-in GLUT emulation
"
HOMEPAGE="https://www.${PN}.org"
DOWNLOADS="${HOMEPAGE}/pub/${PN}/$(ever range 1-3)/${MY_PNV}-source.tar.gz -> ${PNV}.tar.gz"

LICENCES="FLTK"
SLOT="0"
MYOPTIONS="
    ( providers: ijg-jpeg jpeg-turbo ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        dev-libs/libglvnd
        media-libs/fontconfig
        media-libs/freetype:2
        media-libs/libpng:=
        sys-libs/zlib
        x11-dri/glu
        x11-libs/libX11
        x11-libs/libXcursor
        x11-libs/libXext
        x11-libs/libXfixes
        x11-libs/libXft
        x11-libs/libXinerama
        x11-libs/libXrender
        providers:ijg-jpeg? ( media-libs/jpeg:= )
        providers:jpeg-turbo? ( media-libs/libjpeg-turbo )
"

WORK=${WORKBASE}/${MY_PNV}

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --enable-shared
    --enable-threads
    --enable-gl

    --enable-xcursor

    # enable double-buffering
    --enable-xdbe

    --enable-xfixes

    # enable anti-aliased fonts
    --enable-xft

    --enable-xinerama
    --enable-xrender

    --disable-cairo
    # use system libraries
    --disable-local{jpeg,png,zlib}

    --with-x
)

DEFAULT_SRC_INSTALL_PARAMS=( docdir=/usr/share/doc/${PNVR} )

fltk_src_prepare() {
    edo sed -e "s/pkg-config/$(exhost --tool-prefix)pkg-config/" -i configure.ac

    autotools_src_prepare
}

