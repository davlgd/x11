# Copyright 2019, 2021 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require qt cmake [ ninja=true ]

export_exlib_phases src_compile src_install

SUMMARY="Qt Cross-platform application framework: QtCharts"
DESCRIPTION="Qt Charts module provides a set of easy to use chart components.
It uses the Qt Graphics View Framework, therefore charts can be easily
integrated to modern user interfaces. Qt Charts can be used as QWidgets,
QGraphicsWidget, or QML types. Users can easily create impressive graphs by
selecting one of the charts themes."

LICENCES="LGPL-3"

MYOPTIONS="
    examples
    qml [[ description = [ Support for QtQuick and the QML language ] ]]
"

DEPENDENCIES="
    build+run:
        x11-libs/qtbase:${SLOT}[>=${PV}]
        examples? ( x11-libs/qtmultimedia:${SLOT}[>=${PV}] )
        qml? ( x11-libs/qtdeclarative:${SLOT}[>=${PV}] )
"

CMAKE_SRC_CONFIGURE_OPTIONS+=(
    'examples QT_BUILD_EXAMPLES'
)
CMAKE_SRC_CONFIGURE_OPTION_DISABLE_FINDS+=(
    'examples Qt6Multimedia'
    'qml Qt6Qml'
    'qml Qt6Quick'
    'qml Qt6QuickTest'
)

qtcharts-6_src_compile() {
    cmake_src_compile

    option doc && eninja docs
}

qtcharts-6_src_install() {
    cmake_src_install

    option doc && DESTDIR="${IMAGE}" eninja install_docs
}

