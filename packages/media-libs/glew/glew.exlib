# Copyright 2009, 2010, 2011 Ingmar Vanhassel <ingmar@exherbo.org>
# Copyright 2018 Danilo Spinella <danyspin97@protonmail.com>
# Distributed under the terms of the GNU General Public License v2

require github [ user=nigels-com release=${PNV} suffix=tgz ] cmake

export_exlib_phases src_install

SUMMARY="The OpenGL Extension Wrangler Library (GLEW) is a cross-platform open-source C/C++ extension loading library"
DESCRIPTION="
GLEW contains support up to OpenGL 4.6 and the following extensions:
* OpenGL extensions
* WGL extensions
* GLX extensions
"

LICENCES="
    BSD-3
    GPL-2 [[ note = [ Automatic code generation scripts ] ]]
    MIT
"
SLOT="0"
MYOPTIONS="
    X [[ presumed = true ]]
"

DEPENDENCIES="
    build+run:
        dev-libs/libglvnd[X?]
        X? ( x11-libs/libX11 )
    built-against:
        x11-dri/glu[>=9] [[ note = [ Requires: glu in glew.pc ] ]]
"

CMAKE_SOURCE=${WORKBASE}/${PNV}/build/cmake

CMAKE_SRC_CONFIGURE_PARAMS=(
    -DBUILD_UTILS:BOOL=TRUE
)
CMAKE_SRC_CONFIGURE_OPTIONS=(
    'X GLEW_X11'
    '!X GLEW_EGL'

    # Otherwise the FindOpenGL module will try to find libGLX
    '!X OPENGL_USE_EGL'
)

glew_src_install() {
    cmake_src_install

    dodoc ${WORKBASE}/${PNV}/doc/*.{css,html,jpg,png}
}

