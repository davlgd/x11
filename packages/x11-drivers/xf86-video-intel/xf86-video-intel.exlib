# Copyright 2007 Alexander Færøy <ahf@exherbo.org>
# Copyright 2008, 2009, 2010, 2016 Ingmar Vanhassel <ingmar@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

myexparam libdrm

require xorg

if ever is_scm || ever at_least 2.99.917_p20170313; then
    require autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.16 ] ]
fi

export_exlib_phases pkg_setup src_prepare

SUMMARY="Xorg video driver for Intel-based cards"

LICENCES="MIT"
SLOT="0"
MYOPTIONS="
    debug ( providers: eudev systemd ) [[ number-selected = exactly-one ]]
    tools  [[ description = [ Build intel-backlight-helper and intel-virtual-output tool ] ]]
"

DEPENDENCIES="
    build:
        x11-proto/xorgproto
    build+run:
        media-libs/libpng:= [[ note = [ needed for tests ] ]]
        x11-dri/libdrm[>=$(exparam libdrm)][video_drivers:intel(+)]
        x11-dri/mesa[>=7.3_rc2][video_drivers:intel(+)]
        x11-libs/cairo[X]   [[ note = [ needed for tests ] ]]
        x11-libs/libpciaccess[>=0.10]
        x11-libs/libX11[xcb(+)]
        x11-libs/libXext
        x11-libs/libXrandr
        x11-libs/libXrender
        x11-libs/libxshmfence
        x11-libs/libXv
        x11-libs/libXvMC
        x11-libs/libXxf86vm
        x11-libs/libxcb
        x11-libs/pixman:1[>=0.27.1]
        x11-server/xorg-server[>=1.6][dri2(+)]
        x11-utils/xcb-util [[ note = [ works with 0.3.8, too ] ]]
        providers:eudev? ( sys-apps/eudev )
        providers:systemd? ( sys-apps/systemd ) [[ note = [ Automagic dep, enables udev-based monitor hotplug detection ] ]]
        tools? (
            x11-libs/libXcursor
            x11-libs/libXdamage
            x11-libs/libXfixes
            x11-libs/libXinerama
            x11-libs/libXScrnSaver
            x11-libs/libXtst
        ) [[ note = [ Automagic deps for intel-virtual-output ] ]]
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
        --disable-dga
        --disable-dri1
        --disable-xaa
        --enable-backlight
        --enable-dri{,2,3}
        --enable-kms
        --enable-sna
        --enable-udev
        --enable-ums
        --enable-uxa
        --enable-xvmc
        --with-default-dri=3
        --with-default-accel=sna
)
DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=(
    debug
    tools
    'tools backlight-helper'
)

xf86-video-intel_pkg_setup() {
    # Driver uses xf86LoadSubModule to load another xorg module at runtime which means that it
    # contains undefined symbols which would cause loading it to fail when built with -Wl,-z,now
    LDFLAGS+=" -Wl,-z,lazy"
}

xf86-video-intel_src_prepare() {
    # fix socket path for bumblebee.socket
    edo sed \
        -e 's:/var/run:/run:g' \
        -i tools/virtual.c

    autotools_src_prepare
}

