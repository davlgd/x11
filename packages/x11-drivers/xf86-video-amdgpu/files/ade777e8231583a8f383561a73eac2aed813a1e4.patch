Source/Upstream: Yes, fixed in git master

From ade777e8231583a8f383561a73eac2aed813a1e4 Mon Sep 17 00:00:00 2001
From: "Enrico Weigelt, metux IT consult" <info@metux.net>
Date: Tue, 14 May 2024 16:53:54 +0200
Subject: [PATCH] drop compat with ancient xserver versions

We're relying on at least 1.18 now.

Signed-off-by: Enrico Weigelt, metux IT consult <info@metux.net>
---
 src/amdgpu_drv.h      |  6 +-----
 src/amdgpu_glamor.c   |  9 ---------
 src/amdgpu_kms.c      | 10 +---------
 src/amdgpu_probe.c    |  2 --
 src/drmmode_display.c | 21 ---------------------
 src/drmmode_display.h |  2 --
 6 files changed, 2 insertions(+), 48 deletions(-)

diff --git a/src/amdgpu_drv.h b/src/amdgpu_drv.h
index 2952b7b..fbedaaa 100644
--- a/src/amdgpu_drv.h
+++ b/src/amdgpu_drv.h
@@ -355,11 +355,7 @@ Bool amdgpu_dri3_screen_init(ScreenPtr screen);
 Bool amdgpu_window_has_variable_refresh(WindowPtr win);
 Bool amdgpu_scanout_do_update(xf86CrtcPtr xf86_crtc, int scanout_id,
 			      PixmapPtr src_pix, BoxRec extents);
-void AMDGPUWindowExposures_oneshot(WindowPtr pWin, RegionPtr pRegion
-#if XORG_VERSION_CURRENT < XORG_VERSION_NUMERIC(1,16,99,901,0)
-				   , RegionPtr pBSRegion
-#endif
-				   );
+void AMDGPUWindowExposures_oneshot(WindowPtr pWin, RegionPtr pRegion);
 
 /* amdgpu_present.c */
 void amdgpu_present_set_screen_vrr(ScrnInfoPtr scrn, Bool vrr_enabled);
diff --git a/src/amdgpu_glamor.c b/src/amdgpu_glamor.c
index f867392..50ec54f 100644
--- a/src/amdgpu_glamor.c
+++ b/src/amdgpu_glamor.c
@@ -88,15 +88,6 @@ Bool amdgpu_glamor_pre_init(ScrnInfoPtr scrn)
 		return FALSE;
 	}
 
-#if XORG_VERSION_CURRENT < XORG_VERSION_NUMERIC(1,15,0,0,0)
-	if (!xf86LoaderCheckSymbol("glamor_egl_init")) {
-		xf86DrvMsg(scrn->scrnIndex, X_ERROR,
-			   "glamor requires Load \"glamoregl\" in "
-			   "Section \"Module\", disabling.\n");
-		return FALSE;
-	}
-#endif
-
 	/* Load glamor module */
 	if ((glamor_module = xf86LoadSubModule(scrn, GLAMOR_EGL_MODULE_NAME))) {
 		version = xf86GetModuleVersion(glamor_module);
diff --git a/src/amdgpu_kms.c b/src/amdgpu_kms.c
index 882dc33..163efec 100644
--- a/src/amdgpu_kms.c
+++ b/src/amdgpu_kms.c
@@ -1521,11 +1521,7 @@ static void amdgpu_determine_cursor_size(int fd, AMDGPUInfoPtr info)
 }
 
 /* When the root window is mapped, set the initial modes */
-void AMDGPUWindowExposures_oneshot(WindowPtr pWin, RegionPtr pRegion
-#if XORG_VERSION_CURRENT < XORG_VERSION_NUMERIC(1,16,99,901,0)
-				   , RegionPtr pBSRegion
-#endif
-				   )
+void AMDGPUWindowExposures_oneshot(WindowPtr pWin, RegionPtr pRegion)
 {
 	ScreenPtr pScreen = pWin->drawable.pScreen;
 	ScrnInfoPtr pScrn = xf86ScreenToScrn(pScreen);
@@ -1535,11 +1531,7 @@ void AMDGPUWindowExposures_oneshot(WindowPtr pWin, RegionPtr pRegion
 		ErrorF("%s called for non-root window %p\n", __func__, pWin);
 
 	pScreen->WindowExposures = info->WindowExposures;
-#if XORG_VERSION_CURRENT < XORG_VERSION_NUMERIC(1,16,99,901,0)
-	pScreen->WindowExposures(pWin, pRegion, pBSRegion);
-#else
 	pScreen->WindowExposures(pWin, pRegion);
-#endif
 
 	amdgpu_glamor_finish(pScrn);
 	drmmode_set_desired_modes(pScrn, &info->drmmode, TRUE);
diff --git a/src/amdgpu_probe.c b/src/amdgpu_probe.c
index e3f8c08..b6c8312 100644
--- a/src/amdgpu_probe.c
+++ b/src/amdgpu_probe.c
@@ -332,10 +332,8 @@ static Bool AMDGPUDriverFunc(ScrnInfoPtr scrn, xorgDriverFuncOp op, void *data)
 		flag = (CARD32 *) data;
 		(*flag) = 0;
 		return TRUE;
-#if XORG_VERSION_CURRENT > XORG_VERSION_NUMERIC(1,15,99,0,0)
 	case SUPPORTS_SERVER_FDS:
 		return TRUE;
-#endif
        default:
 		return FALSE;
 	}
diff --git a/src/drmmode_display.c b/src/drmmode_display.c
index eea8320..e8267e9 100644
--- a/src/drmmode_display.c
+++ b/src/drmmode_display.c
@@ -611,12 +611,7 @@ drmmode_handle_transform(xf86CrtcPtr crtc)
 {
 	Bool ret;
 
-#if XORG_VERSION_CURRENT >= XORG_VERSION_NUMERIC(1,15,99,903,0)
 	crtc->driverIsPerformingTransform = XF86DriverTransformOutput;
-#else
-	crtc->driverIsPerformingTransform = !crtc->transformPresent &&
-		(crtc->rotation & 0xf) == RR_Rotate_0;
-#endif
 
 	ret = xf86CrtcRotate(crtc);
 
@@ -1623,8 +1618,6 @@ retry:
 	}
 }
 
-#if XORG_VERSION_CURRENT >= XORG_VERSION_NUMERIC(1,15,99,903,0)
-
 static Bool drmmode_load_cursor_argb_check(xf86CrtcPtr crtc, CARD32 * image)
 {
 	if (!drmmode_can_use_hw_cursor(crtc))
@@ -1634,8 +1627,6 @@ static Bool drmmode_load_cursor_argb_check(xf86CrtcPtr crtc, CARD32 * image)
 	return TRUE;
 }
 
-#endif
-
 static void drmmode_hide_cursor(xf86CrtcPtr crtc)
 {
 	ScrnInfoPtr pScrn = crtc->scrn;
@@ -1867,10 +1858,7 @@ static xf86CrtcFuncsRec drmmode_crtc_funcs = {
 	.show_cursor = drmmode_show_cursor,
 	.hide_cursor = drmmode_hide_cursor,
 	.load_cursor_argb = drmmode_load_cursor_argb,
-#if XORG_VERSION_CURRENT >= XORG_VERSION_NUMERIC(1,15,99,903,0)
 	.load_cursor_argb_check = drmmode_load_cursor_argb_check,
-#endif
-
 	.gamma_set = drmmode_crtc_gamma_set,
 	.shadow_create = drmmode_crtc_shadow_create,
 	.shadow_allocate = drmmode_crtc_shadow_allocate,
@@ -2079,7 +2067,6 @@ drmmode_output_mode_valid(xf86OutputPtr output, DisplayModePtr pModes)
 static void
 drmmode_output_attach_tile(xf86OutputPtr output)
 {
-#if XORG_VERSION_CURRENT >= XORG_VERSION_NUMERIC(1, 17, 99, 901, 0)
 	drmmode_output_private_ptr drmmode_output = output->driver_private;
 	drmModeConnectorPtr koutput = drmmode_output->mode_output;
 	AMDGPUEntPtr pAMDGPUEnt = AMDGPUEntPriv(output->scrn);
@@ -2118,7 +2105,6 @@ drmmode_output_attach_tile(xf86OutputPtr output)
 			set = &tile_info;
 	}
 	xf86OutputSetTile(output, set);
-#endif
 }
 
 static int
@@ -2212,9 +2198,7 @@ static void drmmode_output_destroy(xf86OutputPtr output)
 	int i;
 
 	drmModeFreePropertyBlob(drmmode_output->edid_blob);
-#if XORG_VERSION_CURRENT >= XORG_VERSION_NUMERIC(1, 17, 99, 901, 0)
 	drmModeFreePropertyBlob(drmmode_output->tile_blob);
-#endif
 
 	for (i = 0; i < drmmode_output->num_props; i++) {
 		drmModeFreeProperty(drmmode_output->props[i].mode_prop);
@@ -3904,12 +3888,7 @@ restart_destroy:
 	drmmode_validate_leases(scrn);
 
 	if (changed) {
-#if XORG_VERSION_CURRENT >= XORG_VERSION_NUMERIC(1,14,99,2,0)
 		RRSetChanged(xf86ScrnToScreen(scrn));
-#else
-		rrScrPrivPtr rrScrPriv = rrGetScrPriv(scrn->pScreen);
-		rrScrPriv->changed = TRUE;
-#endif
 		RRTellChanged(xf86ScrnToScreen(scrn));
 	}
 
diff --git a/src/drmmode_display.h b/src/drmmode_display.h
index 276d234..462268f 100644
--- a/src/drmmode_display.h
+++ b/src/drmmode_display.h
@@ -157,9 +157,7 @@ typedef struct {
 	drmModeConnectorPtr mode_output;
 	drmModeEncoderPtr *mode_encoders;
 	drmModePropertyBlobPtr edid_blob;
-#if XORG_VERSION_CURRENT >= XORG_VERSION_NUMERIC(1, 17, 99, 901, 0)
 	drmModePropertyBlobPtr tile_blob;
-#endif
 	int dpms_enum_id;
 	int num_props;
 	drmmode_prop_ptr props;
-- 
GitLab

